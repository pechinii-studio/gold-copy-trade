export const color_styles = {
  primary: "#9E7D26",
  bg: "#191505",
  title: "#FFFFFF",
  subtitle: "#AEAEAE",
};

