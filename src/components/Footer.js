import React from "react";
import styled from "styled-components";
import { color_styles } from "../constants/constants";
import LOGO from "../Assets/icons/logo.svg";
import { Col } from "antd";

const FooterWrapper = styled.div`
  width: 100%;
  height: 534px;
  padding: 80px 0px;
  border-top: 1px dashed ${color_styles.primary};
`;

const FooterMenu = styled.li`
  margin-top: 24px;
  color: ${color_styles.subtitle};
  cursor: pointer;

  &:hover {
    opacity: 0.82;
  }
`;

export const Footer = () => {
  return (
    <FooterWrapper>
      <div className="flex">
        <div className="pr-24" style={{ width: "512px" }}>
          <div className="w-80">
            <img src={LOGO} alt="logo" className="object-cover w-full" />
          </div>
          <p className="t-subtitle mt-6">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </p>
        </div>
        <div className="w-full text-subtitle flex justify-between">
          <Col>
            <h5 className="mb-5 font-bold">COMPANY</h5>
            <ul>
              <FooterMenu>Master</FooterMenu>
              <FooterMenu>About us</FooterMenu>
              <FooterMenu>Account type</FooterMenu>
              <FooterMenu>Why us</FooterMenu>
              <FooterMenu>IB Program</FooterMenu>
            </ul>
          </Col>
          <Col>
            <h5 className="mb-5 font-bold">LEARN</h5>
            <ul>
              <FooterMenu>Registration</FooterMenu>
              <FooterMenu>Login</FooterMenu>
              <FooterMenu>Open Live Account</FooterMenu>
              <FooterMenu>Deposit</FooterMenu>
              <FooterMenu>Withdrawal</FooterMenu>
            </ul>
          </Col>
          <Col>
            <h5 className="mb-5 font-bold">RESOURCES</h5>
            <ul>
              <FooterMenu>Support Center</FooterMenu>
              <FooterMenu>Contact us</FooterMenu>
              <FooterMenu>FAQs</FooterMenu>
              <FooterMenu>Partners</FooterMenu>
              <FooterMenu>Updates & News</FooterMenu>
            </ul>
          </Col>
          <Col>
            <h5 className="mb-5 font-bold">MY ACCOUNT</h5>
            <ul>
              <FooterMenu>Profile</FooterMenu>
              <FooterMenu>Trades</FooterMenu>
              <FooterMenu>Followers</FooterMenu>
              <FooterMenu>Providers</FooterMenu>
            </ul>
          </Col>
        </div>
      </div>
      <div className="flex justify-between" style={{ marginTop: "90px" }}>
        <p className="t-subtitle">
          © 2022 Gold Copytrade, All rights reserved.
        </p>
        <p className="t-subtitle">Privacy Policy • Terms & Conditions</p>
      </div>
    </FooterWrapper>
  );
};
