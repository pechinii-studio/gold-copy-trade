import styled from "styled-components";

export const InputGCT = styled.input`
  padding:16px;
  height: 48px;
  outline: none;
  background: linear-gradient(
    180deg,
    rgba(14, 10, 0, 0.25) 0%,
    rgba(20, 20, 20, 0.25) 74.18%
  );
  border: 2px solid rgba(158, 125, 38, 0.25);
  backdrop-filter: blur(15px);
  border-radius: 8px;
`;
