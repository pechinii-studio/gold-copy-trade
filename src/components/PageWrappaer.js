import styled from "styled-components";

export const PageWrapper = styled.div`
  min-width: var(--size-desktop);
  height: auto;
  overflow: hidden;
`;
