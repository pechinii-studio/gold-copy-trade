import { Col, Row } from "antd";
import React from "react";
import styled from "styled-components";
import play_btn from "../Assets/icons/play-btn.png";

const LearnCardWrapper = styled.div`
  display: flex;
  justify-content: center;
  box-sizing: border-box;
  width: 440px;
  height: 490px;
  background: linear-gradient(
    180deg,
    rgba(14, 10, 0, 0.25) 0%,
    rgba(20, 20, 20, 0.25) 74.18%
  );
  border: 2px solid rgba(158, 125, 38, 0.25);
  backdrop-filter: blur(15px);
  border-radius: 16px;
  cursor: pointer;
  transition: transform 400ms ease-in-out 24ms;

  &:hover {
    opacity: 0.95;
    transform: translateX(0px) translateY(-2px);
  }
`;

const InnerLearnCard = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  margin: 16px 0px;
  box-sizing: border-box;
  width: 408px;
  height: 330px;
  border: 1px solid rgba(158, 125, 38, 0.25);
  backdrop-filter: blur(15px);
  border-radius: 16px;
`;

const TagBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 4px 12px;
  background: rgba(206, 183, 125, 0.15);
  border-radius: 6px;
`;

export const LeanCard = ({
  playButton = true,
  url = "",
  tag = "Blog",
  date = "13 Apr 2022",
  name = "What is Bitcoin Halving and Why Does it Matter? (2022 Guide)",
}) => {
  return (
    <LearnCardWrapper>
      <Col>
        <Row className="flex justify-center">
          <InnerLearnCard>
            {playButton && (
              <img
                src={play_btn}
                alt="img"
                className="absolute z-10 cursor-pointer"
                style={{ width: "50px", height: "50px" }}
              />
            )}
            {url !== "" && (
              <img src={url} alt="img" className="object-cover w-full h-full" />
            )}
          </InnerLearnCard>
        </Row>
        <Row className="px-4 mb-1 flex justify-between">
          <TagBox>
            <p className="m-0">{tag}</p>
          </TagBox>
          <p className="t-subtitle">{date}</p>
        </Row>
        <Row
          className="px-4 overflow-hidden text-ellipsis"
          style={{ height: "65px" }}
        >
          <h4>{name}</h4>
        </Row>
      </Col>

      {/* <Row></Row>
      <Col className="flex justify-between">
        <div>Blog</div>
        <p>13 Apr 2022</p>
      </Col>
      <Row className="w-full p-4">
        <h4>What is Bitcoin Halving and Why Does it Matter? (2022 Guide)</h4>
      </Row> */}
    </LearnCardWrapper>
  );
};
