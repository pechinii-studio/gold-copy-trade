import styled from "styled-components";
import { color_styles } from "../constants/constants"

export const ButtonGCT = styled.button`
  color: white;
  background-color: ${(props) =>
    props.outline
      ? "none"
      : props.secondary
      ? `rgba(206, 183, 125, 0.15)`
      : `${color_styles.primary}`};
  border: none;
  outline-offset: ${(props) => (props.outline ? `-2px` : "none")};
  outline: ${(props) =>
    props.outline ? `2px solid ${color_styles.primary}` : "none"};
  color: white;
  padding: 12px 36px;
  border-radius: 100px;
  font-size: 16px;

  &:hover{
    opacity: 0.9;
  }
`;
