import styled from "styled-components";

export const OfferCard = styled.div`
  padding: 48px;
  width: 410px;
  height: 710px;
  background: ${(props) =>
    props.primary
      ? `linear-gradient(180deg, rgba(255, 196, 42, 0.25) 0%, rgba(20, 20, 20, 0.25) 74.18%)`
      : `linear-gradient(
    180deg,
    rgba(14, 10, 0, 0.25) 0%,
    rgba(20, 20, 20, 0.25) 74.18%
  )`};
  border: 2px solid rgba(158, 125, 38, 0.25);
  backdrop-filter: blur(15px);
  border-radius: 16px;
  cursor: pointer;
  transition: transform 400ms ease-in-out 24ms;

  &:hover {
    opacity: 0.95;
    transform: translateX(0px) translateY(-4px);
  }
`;
