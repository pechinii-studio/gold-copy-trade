import { Col, Row } from "antd";
import React from "react";
import styled from "styled-components";
import star_provider from "../Assets/icons/star-provider.svg";
import { ProviderList } from "./ProviderList";

const ProviderPlusWrapper = styled.div`
    margin: 32px 0px;
  position: relative;
  padding: 60px;
  width: 1320px;
  height: 1360px;
  background: linear-gradient(
    180deg,
    rgba(255, 196, 42, 0.25) 0%,
    rgba(20, 20, 20, 0.25) 74.18%
  );
  border: 2px solid rgba(158, 125, 38, 0.25);
  backdrop-filter: blur(15px);
  /* Note: backdrop-filter has minimal browser support */

  border-radius: 16px;
`;

export const ProviderPlus = () => {
  return (
    <ProviderPlusWrapper>
      <Row className="absolute top-11 right-11">
        <img src={star_provider} alt="i" />
      </Row>
      <Row>
        <Col>
          <h2>Provider+</h2>
          <div style={{ width: "420px" }}>
            <p className="t-subtitle">
              You can add more options to gain extra benefits by using provider+
              account
            </p>
          </div>
        </Col>
      </Row>
      <Row className="mt-6">
        <ProviderList
          Type="Subscription fee"
          Mode="Percentage (%) of the allocated amount"
          Frequency="Once, at the time of investment"
          Example="If broker SF is 1%, fund manager SF is 2%, then total investors charge 3%. If Investor allocates 1,000 USD, he will be charged -30 USD at the time of investment and equity of allocation will be 970 USD."
        />
        <ProviderList
          Type="Management fee"
          Mode="Percentage (%) of the balance or equity of the allocation, estimated as an annual charge"
          Frequency="At the time of investment and then on monthly basis at the set date & time defined by broker"
          Example="If broker MF is 0.5% and fund manager MF is 0.5%, then total investor charge 1%. If investor allocates 1,000 USD then his charge: 1,000 * (1% / 12) = 0.83 USD will be deducted at the time of investment. If the balance/equity of the next month deduction time is 1,500 USD, then next month charge will be: 1,500 * (1% / 12) = 1.24 USD"
        />
        <ProviderList
          Type="Performance fee"
          Mode="Percentage (%) of balance or equity of the allocation"
          Frequency="Monthly, at the set date & time defined by broker or in the event of stop allocation"
          Example="Charged only in case of a positive performance. Calculation is based on the HWM (High Water Mark) - only the new accumulated positive performance will be charged. Balance based calculation applies only for closed positions Equity based calculation applies for both closed and open positions"
        />
        <ProviderList
          Type="Exit fee"
          Mode="Percentage (%) of the equity of allocation at the moment of stop"
          Frequency="Upon stop allocation / stop-out"
          Example="If broker EF is 1% and fund manager EF is 1%, then total investor charge 2%. If investor stops allocation or gets stopped out, he will be charged 2% of the equity of allocation at the moment of stop."
        />
        <ProviderList
          Type="Fixed fee"
          Mode="Set amount in the currency of the fund"
          Frequency="Monthly at the set date & time defined by broker, on a day count basis."
          Example="If broker FF is 2 USD and fund manager FF is 2 USD, then total investor charge 4 USD.
          Investor will be always charged only for the number of days when allocation was active before the calculation time."
          UnderLine={false}
        />
      </Row>
    </ProviderPlusWrapper>
  );
};
