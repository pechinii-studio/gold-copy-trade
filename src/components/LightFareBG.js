import styled from "styled-components";

export const LightFareBG = styled.div`
  position: absolute;
  z-index: 2;
  background: linear-gradient(
    192.16deg,
    rgba(206, 183, 125, 0.45) 0%,
    rgba(90, 65, 0, 0.45) 118.82%
  );
  filter: blur(500px);

`;
