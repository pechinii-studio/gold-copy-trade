import { Col, Row } from "antd";
import React from "react";
import styled from "styled-components";
import { ButtonGCT } from "./ButtonGCT";

import JoinUs01 from "../Assets/images/join-01.png";
import JoinUs02 from "../Assets/images/join-02.png";
import JoinUs03 from "../Assets/images/join-03.png";
import JoinUs04 from "../Assets/images/join-04.png";
import JoinUs05 from "../Assets/images/join-05.png";
import JoinUs06 from "../Assets/images/join-06.png";

export const TextBoxStayInLoopWrap = styled.div`
  width: 500px;
  height: 56px;
  background: linear-gradient(
    180deg,
    rgba(14, 10, 0, 0.25) 0%,
    rgba(20, 20, 20, 0.25) 74.18%
  );
  border: 2px solid rgba(158, 125, 38, 0.25);
  backdrop-filter: blur(15px);
  border-radius: 16px;
`;

export const TextBoxStayInLoop = styled.input`
  width: 320px;
  height: 100%;
  padding: 16px 0px 16px 28px;
  font-size: 16px;
  border: none;
  background-color: transparent;
  outline: none;
`;

export const JoinOur = () => {
  return (
    <Row className="flex justify-between mt-10 mb-16">
      <Col>
        <h2>Stay in loop</h2>
        <div style={{ width: "460px" }}>
          <p className="t-subtitle">
            By submitting your email you agree to our Terms of Use and Sale and
            Privacy Policy. You will receive email communications from us for
            marketing, informational, and promotional purposes and can opt-out
            at any time.
          </p>
        </div>
        <TextBoxStayInLoopWrap className="mt-6 flex justify-between items-center">
          <TextBoxStayInLoop placeholder="Enter Email Address" />
          <ButtonGCT
            className="flex justify-center items-center mr-2"
            style={{ width: "140px", height: "42px" }}
          >
            Subscribe
          </ButtonGCT>
        </TextBoxStayInLoopWrap>
      </Col>
      <Col>
        <h2>Join our community</h2>
        <Row className="mt-4">
          <Col
            className="mr-3 ml-3 flex justify-center items-center overflow-hidden"
            style={{ width: "49px", height: "49px" }}
          >
            <img src={JoinUs01} alt="i" className="object-cover" />
          </Col>
          <Col
            className="mr-3 ml-3 flex justify-center items-center overflow-hidden"
            style={{ width: "49px", height: "49px" }}
          >
            <img src={JoinUs02} alt="i" className="object-cover" />
          </Col>
          <Col
            className="mr-3 ml-3 flex justify-center items-center overflow-hidden"
            style={{ width: "49px", height: "49px" }}
          >
            <img src={JoinUs03} alt="i" className="object-cover" />
          </Col>
          <Col
            className="mr-3 ml-3 flex justify-center items-center overflow-hidden"
            style={{ width: "49px", height: "49px" }}
          >
            <img src={JoinUs04} alt="i" className="object-cover" />
          </Col>
          <Col
            className="mr-3 ml-3 flex justify-center items-center overflow-hidden"
            style={{ width: "49px", height: "49px" }}
          >
            <img src={JoinUs05} alt="i" className="object-cover" />
          </Col>
          <Col
            className="mr-3 ml-3 flex justify-center items-center overflow-hidden"
            style={{ width: "49px", height: "49px" }}
          >
            <img src={JoinUs06} alt="i" className="object-cover" />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
