import styled from "styled-components";

export const LightFareDark = styled.div`
  position: absolute;
  z-index: 2;
  width: 611px;
  height: 611px;
  background: rgba(0, 0, 0);
  filter: blur(500px);
`;
