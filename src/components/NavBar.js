import styled from "styled-components";
import LOGO from "../Assets/icons/logo.svg";
// import Keyword_Icon from "../Assets/icons/keywords-icon.svg";
// import Regulated_Icon from "../Assets/icons/regulated-icon.svg";
// import Segregated_Icon from "../Assets/icons/segregated-icon.svg";
// import Support_Icon from "../Assets/icons/support-icon.svg";
import EN from "../Assets/icons/EN.svg";
import { ButtonGCT } from "./ButtonGCT";
import { IoIosArrowDown } from "react-icons/io";
import { Link } from "react-router-dom";
import { Menu, Row } from "antd";
import { useState } from "react";

const Nav = styled.div`
  width: 100%;
  height: 130px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: relative;
  z-index: 15;
`;

const items = [
  {
    label: <Link to="/master">Master</Link>,
    key: "master",
  },
  {
    label: (
      <Row className="flex items-center" style={{ color: "white" }}>
        <Link to="/account-type">
          <p style={{ color: "white" }} className="mb-0 mr-2">
            Account type
          </p>
        </Link>
        <IoIosArrowDown />
      </Row>
    ),
    key: "account",
    children: [
      {
        label: <Link to="/follower-account-type">Follower Account Type</Link>,
        key: "follwer",
      },
    ],
  },
  {
    label: <Link to="/why-us">Why us</Link>,
    key: "why-us",
  },
  {
    label: <Link to="/learn">Learn</Link>,
    key: "learn",
  },
  {
    label: <Link to="/ib-program">IB Program</Link>,
    key: "ib",
  },
  {
    label: <Link to="/contact-us">Contact us</Link>,
    key: "contact",
  },
  {
    label: (
      <Row className="flex items-center">
        <img src={EN} alt="i" />
        <p className="mb-0 mx-2">EN</p>
        <IoIosArrowDown />
      </Row>
    ),
    key: "language",
    children: [
      {
        label: "EN",
        key: "en",
      },
      {
        label: "TH",
        key: "th",
      },
    ],
  },
];

export const NavBar = () => {
  const [current, setCurrent] = useState("home");

  const onClick = (e) => {
    console.log("click ", e);
    setCurrent(e.key);
  };

  return (
    <Nav>
      <Link to="/">
        <div>
          <img src={LOGO} alt="logo" />
        </div>
      </Link>
      <div className="flex items-center">
        <div className="flex items-center">
          <Menu
            theme="dark"
            style={{
              width: "750px",
              backgroundColor: "transparent",
              border: "none",
            }}
            onClick={onClick}
            selectedKeys={[current]}
            mode="horizontal"
            items={items}
          />
        </div>
        <div className="flex">
          <ButtonGCT outline className="flex items-center mr-6">
            <p className="mb-0 mr-2">Register</p>
            <IoIosArrowDown />
          </ButtonGCT>
          <ButtonGCT primary>Login</ButtonGCT>
        </div>
      </div>
    </Nav>
  );
};
