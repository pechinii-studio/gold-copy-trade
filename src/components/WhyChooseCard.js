import React from "react";
import styled from "styled-components";
import Keywords_Icon from "../Assets/icons/keywords-icon.svg";

const WhyChooseCardStyles = styled.div`
  overflow: hidden;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 227px;
  height: 216px;
  background: linear-gradient(
    180deg,
    rgba(14, 10, 0, 0.25) 0%,
    rgba(20, 20, 20, 0.25) 74.18%
  );
  border: 2px solid rgba(158, 125, 38, 0.25);
  backdrop-filter: blur(15px);
  border-radius: 16px;
  transition: transform 480ms ease-in-out 24ms;

  &:hover {
    opacity: 0.94;
    transform: translateX(0px) translateY(-4px);
  }
`;

export const WhyChooseCard = ({
  image = Keywords_Icon,
  name = "Keywords in domain name",
}) => {
  return (
    <WhyChooseCardStyles>
      <div className="w-20 h-20 overflow-hidden flex justify-center items-center">
        <img src={image} alt="icon" className="object-cover" />
      </div>
      <div className="mt-4 w-4/5 h-16 break-normal text-center text-ellipsis overflow-hidden">
        <h4>{name}</h4>
      </div>
    </WhyChooseCardStyles>
  );
};
