import { Col, Row } from "antd";
import React from "react";
import styled from "styled-components";
import { ProviderList } from "./ProviderList";

const ProviderWrapper = styled.div`
  margin: 32px 0px;
  padding: 60px;
  width: 1320px;
  height: 400px;
  background: linear-gradient(
    180deg,
    rgba(14, 10, 0, 0.25) 0%,
    rgba(20, 20, 20, 0.25) 74.18%
  );
  border: 2px solid rgba(158, 125, 38, 0.25);
  backdrop-filter: blur(15px);
  border-radius: 16px;
`;

export const Provider = () => {
  return (
    <ProviderWrapper>
      <Row>
        <Col>
          <h2>Provider</h2>
          <div style={{ width: "460px" }}>
            <p className="t-subtitle">
              Followers can close orders by themselves. And the master receives
              the only commission which is the Subscription fee.
            </p>
          </div>
        </Col>
      </Row>
      <ProviderList
        UnderLine={false}
        Type="Subscription fee"
        Mode="Percentage (%) of the allocated amount"
        Frequency="Once, at the time of investment"
        Example="If broker SF is 1%, fund manager SF is 2%, then total investors charge 3%. If Investor allocates 1,000 USD, he will be charged -30 USD at the time of investment and equity of allocation will be 970 USD."
      />
    </ProviderWrapper>
  );
};
