import React from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import { FaRegHeart } from "react-icons/fa";
import ChartTrader01 from "../Assets/vectors/chart-trader-01.svg";

const TraderCardStyles = styled.div`
  cursor: pointer;
  position: relative;
  overflow: hidden;
  padding: 8px 24px;
  width: 310px;
  height: 248px;
  background: linear-gradient(
    180deg,
    rgba(14, 10, 0, 0.25) 0%,
    rgba(20, 20, 20, 0.25) 74.18%
  );
  border: 2px solid rgba(158, 125, 38, 0.25);
  backdrop-filter: blur(15px);
  border-radius: 16px;
  transition: transform 480ms ease-in-out 24ms;

  &:hover {
    opacity: 0.94;
    transform: translateX(0px) translateY(-12px);
  }
`;

export const TraderCard = ({
  image = "https://api.lorem.space/image/face?w=150&h=150",
  name = "Name",
  username = "@username",
  gain = "0.00%",
  follower = "0",
  amount = "$0.00",
  chart = ChartTrader01,
}) => {
  return (
    <TraderCardStyles>
      <Row className="h-20 flex justify-between">
        <Row className="flex items-center" style={{ width: "220px" }}>
          <Col className="w-12 h-12 mr-5 mb-4 rounded-full overflow-hidden flex items-center justify-center">
            <img src={image} alt="img" className="object-cover w-16" />
          </Col>
          <Col className="truncate" style={{ width: "150px" }}>
            <h4 className="leading-4">{name}</h4>
            <h5 className="t-primary leading-4">{username}</h5>
          </Col>
        </Row>
        <FaRegHeart size={18} className="mt-2" />
      </Row>
      <Row className="flex justify-between">
        <Col className="flex-col justify-center items-center">
          <h5 className="t-label leading-4">Gain</h5>
          <h5>{gain}</h5>
        </Col>
        <Col className="flex-col justify-center items-center">
          <h5 className="t-label leading-4">Follower</h5>
          <h5>{follower}</h5>
        </Col>
        <Col className="flex-col justify-center items-center">
          <h5 className="t-label leading-4">Amount</h5>
          <h5>{amount}</h5>
        </Col>
      </Row>
      <Row className="absolute bottom-0 left-0">
        <img src={chart} alt="chart" />
      </Row>
    </TraderCardStyles>
  );
};
