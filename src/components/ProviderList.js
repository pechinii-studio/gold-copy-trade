import { Col, Row } from "antd";
import React from "react";
import styled from "styled-components";

const ProviderLine = styled.div`
  margin-top: 40px;
  width: 100%;
  border-bottom: 1px dashed #9e7d26;
`;
export const ProviderList = ({
  Type = "--Type--",
  Mode = "--Mode--",
  Frequency = "--Frequency--",
  Example = "--Example--",
  UnderLine = true,
}) => {
  return (
    <Row className="w-full mt-10">
      <Col style={{ width: "20%" }}>
        <p className="t-subtitle">Type</p>
        <p className="opacity-90 pr-10">{Type}</p>
      </Col>
      <Col style={{ width: "20%" }}>
        <p className="t-subtitle">Mode</p>
        <p className="opacity-90 pr-10">{Mode}</p>
      </Col>
      <Col style={{ width: "20%" }}>
        <p className="t-subtitle">Type</p>
        <p className="opacity-90 pr-10">{Frequency}</p>
      </Col>
      <Col style={{ width: "40%" }}>
        <p className="t-subtitle">Example</p>
        <p className="opacity-90 pr-2">{Example}</p>
      </Col>
      {UnderLine && <ProviderLine/>}
    </Row>
  );
};
