import { Col, Row } from "antd";
import React from "react";
import CorrectIcon from "../Assets/icons/correct-icon.svg";
import IncorrectIcon from "../Assets/icons/incorrect-icon.svg";
import "../pages/HomePage/HomePageStyles.css";

export const OfferList = ({ status = true, name = "", props }) => {
  return (
    <Row className="flex items-center mt-2">
      <Col className="check-list-wrap mr-3">
        <img src={status ? CorrectIcon : IncorrectIcon} alt="i" />
      </Col>
      <Col>{name}</Col>
    </Row>
  );
};
