import React from "react";
import styled from "styled-components";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
const ButtonWrapper = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 46px;
  height: 46px;
  margin: 8px;
  background: rgba(206, 183, 125, 0.15);
  border-radius: 100px;

  &:hover{
    background: rgba(206, 183, 125, 0.30);
  }
`;
export const PaginationButton = ({
  text = "...",
  arrowLeft = false,
  arrowRight = false,
}) => {
  return (
    <ButtonWrapper>
      {arrowLeft === false && arrowRight === false ? (
        text
      ) : arrowLeft ? (
        <IoIosArrowBack />
      ) : arrowRight ? (
        <IoIosArrowForward />
      ) : (
        ""
      )}
    </ButtonWrapper>
  );
};
