import "./App.less";
import { HomePage } from "./pages/HomePage/HomePage";
import { Route, BrowserRouter, Routes } from "react-router-dom";
import { MasterPage } from "./pages/Master/MasterPage";
import ContactUs from "./pages/ContactUs/ContactUs";
import { IBPage } from "./pages/IB/IBPage";
import { AccountTypePage } from "./pages/AccountType/AccountTypePage";
import { WhyUsPage } from "./pages/WhyUs/WhyUsPage";
import { FollowerPage } from "./pages/FollowerPage/FollowerPage";
import { LearnPage } from "./pages/Learn/LearnPage";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />}></Route>
          <Route path="/master" element={<MasterPage />}></Route>
          <Route path="/account-type" element={<AccountTypePage />}></Route>
          <Route
            path="/follower-account-type"
            element={<FollowerPage />}
          ></Route>
          <Route path="/why-us" element={<WhyUsPage />}></Route>
          <Route path="/learn" element={<LearnPage />}></Route>
          <Route path="/ib-program" element={<IBPage />}></Route>
          <Route path="/contact-us" element={<ContactUs />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
