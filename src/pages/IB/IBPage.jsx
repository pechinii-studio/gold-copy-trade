import { Col, Row } from "antd";
import React from "react";
import IB_BG from "../../Assets/images/ib-program/ib-bg.png";
import play_icon from "../../Assets/icons/play-icon.svg";
import { Footer } from "../../components/Footer";
import { JoinOur } from "../../components/JoinOur";
import { NavBar } from "../../components/NavBar";
import { ButtonGCT } from "../../components/ButtonGCT";
import { VectorBg } from "../../components/VectorBG";
import CurveBG from "../../Assets/vectors/bg-curve-master.svg";
import { PageWrapper } from "../../components/PageWrappaer";

export const IBPage = () => {
  return (
    <PageWrapper>
      <NavBar />
      <Row className="flex justify-center w-full">
        <VectorBg src={CurveBG} alt="img" style={{ top: "300px" }} />
      </Row>
      <Row>
        <VectorBg
          className="w-full absolute"
          src={IB_BG}
          alt="img"
          style={{ top: "120px", right: "0px", zIndex: "0" }}
        />
      </Row>
      <Row className="mt-20 relative z-10" style={{ height: "700px" }}>
        <Row className="w-full">
          <Col style={{ width: "540px" }}>
            <h2>IB Program</h2>
            <div style={{ width: "360px" }}>
              <p>
                Earn commissions every month without additional conditions.
                Become Gold Copytrade’s IB.
              </p>
            </div>

            <p className="mt-6 t-subtitle">
              Gold Copytrade offers one of the most generous Introducing Broker
              (IB) program in the industry. Our program allows organizations and
              individuals around the globe to be remunerated for introducing new
              clients to us. We offer a total solution, from the provision of
              the trading platforms to execution and settlement of transactions.
              you can achieve success in a short time. You can attract clients
              from all over the world without limits, so your earning potential
              has no boundaries.
            </p>
            <p className="mt-6 t-subtitle">
              An account manager is appointed to each IB to help them develop
              their business and to ensure we provide the highest levels of
              service. We take care of all the administration, allowing you, our
              IB, to focus on the client
            </p>
          </Col>
        </Row>
        <Row className="flex mb-52">
          <ButtonGCT className="mr-6 h-12">Invest Now</ButtonGCT>
          <ButtonGCT secondary className="flex items-center h-12">
            <img src={play_icon} alt="i" className="mr-2" />
            Learn more
          </ButtonGCT>
        </Row>
      </Row>

      <JoinOur />
      <Footer />
    </PageWrapper>
  );
};
