import { Row, Col } from "antd";
import React from "react";
import { Footer } from "../../components/Footer";
import { JoinOur } from "../../components/JoinOur";
import { NavBar } from "../../components/NavBar";
import { PageWrapper } from "../../components/PageWrappaer";
import { Provider } from "../../components/Provider";
import { ProviderPlus } from "../../components/ProviderPlus";
import { VectorBg } from "../../components/VectorBG";
import CurveBG from "../../Assets/vectors/bg-curve-master.svg";

export const AccountTypePage = () => {
  return (
    <PageWrapper>
      <NavBar />
      <Row className="flex justify-center w-full">
        <VectorBg
          src={CurveBG}
          alt="img"
          style={{ top: "-100px" }}
        />
      </Row>
      <Row className="flex justify-center mt-16 mb-24 relative z-10" style={{ width: "1400px" }}>
        <Col className="mb-4">
          <h2>Lifetime Master Account</h2>
          <div className="w-full flex justify-center">
            <div className=" text-center" style={{ width: "380px" }}>
              <p className="t-subtitle">
                Gain unrestricted access to our analytics platform and enjoy the
                perks offered through Gold Coptrade.
              </p>
            </div>
          </div>
        </Col>
        <Provider />
        <ProviderPlus className="mt-10" />
      </Row>
      <JoinOur />
      <Footer />
    </PageWrapper>
  );
};
