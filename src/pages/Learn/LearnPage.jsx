import { Col, Row } from "antd";
import React from "react";
import { Footer } from "../../components/Footer";
import { JoinOur } from "../../components/JoinOur";
import { LeanCard } from "../../components/LeanCard";
import { NavBar } from "../../components/NavBar";
import { VectorBg } from "../../components/VectorBG";
import CurveBG from "../../Assets/vectors/bg-curve-master.svg";
import { PageWrapper } from "../../components/PageWrappaer";
import Learn_Img_01 from "../../Assets/images/learn/learn-img-01.png";
import Learn_Img_02 from "../../Assets/images/learn/learn-img-02.png";
import Learn_Img_03 from "../../Assets/images/learn/learn-img-03.png";
import Learn_Img_04 from "../../Assets/images/learn/learn-img-04.png";
import Learn_Img_05 from "../../Assets/images/learn/learn-img-05.png";
import Learn_Img_06 from "../../Assets/images/learn/learn-img-06.png";
import Learn_Img_07 from "../../Assets/images/learn/learn-img-07.png";
import Learn_Img_08 from "../../Assets/images/learn/learn-img-08.png";
import Learn_Img_09 from "../../Assets/images/learn/learn-img-09.png";
import Learn_Img_10 from "../../Assets/images/learn/learn-img-10.png";
import Learn_Img_11 from "../../Assets/images/learn/learn-img-11.png";
import Learn_Img_12 from "../../Assets/images/learn/learn-img-12.png";
import { PaginationButton } from "../../components/PaginationButton";

export const LearnPage = () => {
  return (
    <PageWrapper>
      <NavBar />
      <Row className="flex justify-center w-full">
        <VectorBg src={CurveBG} alt="img" style={{ top: "-10px" }} />
      </Row>
      <Row
        className="flex justify-center mt-16 mb-24 relative z-10"
        style={{ width: "1400px" }}
      >
        <Col className="mb-4">
          <h2>Lifetime Master Account</h2>
          <div className="w-full flex justify-center">
            <div className=" text-center" style={{ width: "380px" }}>
              <p className="t-subtitle">
                Gain unrestricted access to our analytics platform and enjoy the
                perks offered through Gold Coptrade.
              </p>
            </div>
          </div>
        </Col>
        <Col className="w-full mt-10 mb-10">
          <Row className="flex justify-between mb-10">
            <LeanCard
              url={Learn_Img_01}
              tag="Blog"
              date="13 Apr 2022"
              name="What is Bitcoin Halving and Why Does it Matter? (2022 Guide)"
            />
            <LeanCard
              url={Learn_Img_02}
              tag="News"
              date="13 Apr 2022"
              name="Gold Copytrade is  Joining hands with Finvasia Group tomorrow"
            />
            <LeanCard
              url={Learn_Img_03}
              tag="Media"
              date="13 Apr 2022"
              name="Which Gold Copy Trade Products are Shining in Stock market?"
            />
          </Row>
          <Row className="flex justify-between mb-10">
            <LeanCard
              url={Learn_Img_04}
              tag="Updates"
              date="13 Apr 2022"
              name="Bitcoin vs Bitcoin Cash: 5 Differences Investors Should Know"
            />
            <LeanCard
              url={Learn_Img_05}
              tag="Blog"
              date="13 Apr 2022"
              name="6 Strategies for Day Trading Crypto (Guide for 2022)"
            />
            <LeanCard
              url={Learn_Img_06}
              tag="Blog"
              date="13 Apr 2022"
              name="28 Crypto Terms to Know Before You Invest in Crypto (2022 List)"
            />
          </Row>
          <Row className="flex justify-between mb-10">
            <LeanCard
              url={Learn_Img_07}
              tag="Blog"
              date="13 Apr 2022"
              name="What is Bitcoin Halving and Why Does it Matter? (2022 Guide)"
            />
            <LeanCard
              url={Learn_Img_08}
              tag="News"
              date="13 Apr 2022"
              name="Gold Copytrade is  Joining hands with Finvasia Group tomorrow"
            />
            <LeanCard
              url={Learn_Img_09}
              tag="Media"
              date="13 Apr 2022"
              name="Which Gold Copy Trade Products are Shining in Stock market?"
            />
          </Row>
          <Row className="flex justify-between mb-10">
            <LeanCard
              url={Learn_Img_10}
              tag="Updates"
              date="13 Apr 2022"
              name="Bitcoin vs Bitcoin Cash: 5 Differences Investors Should Know"
            />
            <LeanCard
              url={Learn_Img_11}
              tag="Blog"
              date="13 Apr 2022"
              name="6 Strategies for Day Trading Crypto (Guide for 2022)"
            />
            <LeanCard
              url={Learn_Img_12}
              tag="Blog"
              date="13 Apr 2022"
              name="28 Crypto Terms to Know Before You Invest in Crypto (2022 List)"
            />
          </Row>
          <Row className="flex justify-center">
            <PaginationButton arrowLeft/>
            <PaginationButton text="1"/>
            <PaginationButton text="2"/>
            <PaginationButton text="3"/>
            <PaginationButton text="..."/>
            <PaginationButton text="16"/>
            <PaginationButton arrowRight/>
          </Row>
        </Col>
      </Row>
      <JoinOur />
      <Footer />
    </PageWrapper>
  );
};
