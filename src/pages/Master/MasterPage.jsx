import { Col, Row } from "antd";
import React from "react";
import styled from "styled-components";
import { ButtonGCT } from "../../components/ButtonGCT";
import { Footer } from "../../components/Footer";
import { InnerContent } from "../../components/InnerContent";
import { NavBar } from "../../components/NavBar";
import play_icon from "../../Assets/icons/play-icon.svg";
import { JoinOur } from "../../components/JoinOur";
import CheckIcon from "../../Assets/icons/icon-check.svg";
import WindowsImage from "../../Assets/images/master-page/windows-img.png";
import { VectorBg } from "../../components/VectorBG";
import CurveBG from "../../Assets/vectors/bg-curve-master.svg";
import { LightFareDark } from "../../components/LightFareDark";
import BodyImage from "../../Assets/images/master-page/body-img2.png";
import { PageWrapper } from "../../components/PageWrappaer";

const MasterPageContent = styled.div`
  height: 700px;
  position: relative;
  overflow: hidden;
`;

export const MasterPage = () => {
  return (
    <PageWrapper>
      <NavBar />
      <Row className="flex justify-center w-full">
        <VectorBg src={CurveBG} alt="img" style={{ top: "280px" }} />
      </Row>
      <LightFareDark
        style={{
          width: "100%",
          height: "600px",
          right: "0px",
          opacity: "0.85",
          position: "absolute",
          zIndex: "0",
        }}
      />

      <MasterPageContent>
        <VectorBg
          src={BodyImage}
          alt="img"
          style={{ width: "900px", top: "-20px", right: "0px", zIndex: "0" }}
        />
        <InnerContent>
          <Row className="mt-16" style={{ width: "680px" }}>
            <h1>Become Master</h1>
            <h5 className="t-subtitle" style={{ width: "600px" }}>
              If you are someone who has a great trading strategy. and can
              survive in gold trading Don't wait, Hurry up to apply to become a
              master with us. You will receive many special privileges from
              Followers copying you.
            </h5>
            <h5 className="mt-5 t-subtitle" style={{ width: "600px" }}>
              Trade as usual and earn additional income from others copying you.
              Your provider profile displays daily and monthly stats on your
              trading performance—promote it and attract new followers!
            </h5>
          </Row>
          <Row className="flex mt-10">
            <ButtonGCT className="mr-6">Invest Now</ButtonGCT>
            <ButtonGCT secondary className="flex items-center">
              <img src={play_icon} alt="i" className="mr-2" />
              Learn more
            </ButtonGCT>
          </Row>
        </InnerContent>
      </MasterPageContent>
      <Row className="mt-10 relative" style={{ height: "620px" }}>
        <InnerContent>
          <Row className="flex justify-start">
            <Col
              className="simple-steps-wrap-bottom mr-16"
              style={{ width: "740px" }}
            >
              <img
                src={WindowsImage}
                alt="img"
                className="object-cover w-full"
              />
            </Col>
            <Col className="mt-4 pr-12" style={{ width: "540px" }}>
              <Row>
                <Col>
                  <Row>
                    <h2>How it</h2>
                    <h2 className="t-primary ml-4">works.</h2>
                  </Row>
                  <p className="t-subtitle">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam.
                  </p>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col className="mr-6 btn-check-wrap">
                  <img src={CheckIcon} alt="i" />
                </Col>
                <Col style={{ width: "400px" }}>
                  <h5>Create Master Account</h5>
                  <p className="t-subtitle">
                    Click on Master Area and create a Master Account—start a new
                    one or assign an existing one as your master account.
                  </p>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col className="mr-6 btn-check-wrap">
                  <img src={CheckIcon} alt="i" />
                </Col>
                <Col style={{ width: "400px" }}>
                  <h5>Create Allocation</h5>
                  <p className="t-subtitle">
                    Get your Master Account ready for copiers: set your
                    commission amount and describe your strategy.
                  </p>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col className="mr-6 btn-check-wrap">
                  <img src={CheckIcon} alt="i" />
                </Col>
                <Col style={{ width: "400px" }}>
                  <h5>Investors select the Traders to copy</h5>
                  <p className="t-subtitle">
                    Use your Master Area to view detailed statistics on your
                    trading, change your account settings, and view the amount
                    of commission you’ve earned.
                  </p>
                </Col>
              </Row>
            </Col>
          </Row>
        </InnerContent>
      </Row>
      <JoinOur />
      <Footer />
    </PageWrapper>
  );
};
