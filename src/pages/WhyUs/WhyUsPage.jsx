import { Col, Row } from "antd";
import React from "react";
import { ButtonGCT } from "../../components/ButtonGCT";
import { Footer } from "../../components/Footer";
import { JoinOur } from "../../components/JoinOur";
import { NavBar } from "../../components/NavBar";
import { PageWrapper } from "../../components/PageWrappaer";
import play_icon from "../../Assets/icons/play-icon.svg";
import G_FG from "../../Assets/images/why-us/G_FG.png";
import G_BG from "../../Assets/images/why-us/G_BG.png";
import WindowsImage from "../../Assets/images/master-page/windows-img.png";
import CheckIcon from "../../Assets/icons/icon-check.svg";
import { VectorBg } from "../../components/VectorBG";
import CurveBG from "../../Assets/vectors/bg-curve-master.svg";

export const WhyUsPage = () => {
  return (
    <PageWrapper>
      <NavBar />
      <Row className="flex justify-center w-full">
        <VectorBg
          src={CurveBG}
          alt="img"
          style={{ top: "260px" }}
        />
      </Row>
      <Row className="mt-16" style={{ width: "1400px" }}>
        <Row>
          <Col className="mr-20">
            <Row>
              <h2>Why</h2>
              <h2 className="t-primary ml-4">Gold Copytrade</h2>
            </Row>
            <div style={{ width: "650px" }}>
              <p className="t-subtitle mt-3">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae
                vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi
                nesciunt.
              </p>
              <p className="t-subtitle mt-6 mb-16">
                Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                consectetur, adipisci velit, sed quia non numquam eius modi
                tempora incidunt ut labore et dolore magnam aliquam quaerat
                voluptatem. Ut enim ad minima veniam, quis nostrum
                exercitationem ullam corporis suscipit laboriosam, nisi ut
                aliquid ex ea commodi consequatur? Quis autem vel eum iure
                reprehenderit qui in ea voluptate velit esse quam nihil
                molestiae consequatur.
              </p>
            </div>

            <Row className="flex mb-20">
              <ButtonGCT className="mr-6 h-12">Invest Now</ButtonGCT>
              <ButtonGCT secondary className="flex items-center h-12">
                <img src={play_icon} alt="i" className="mr-2" />
                Learn more
              </ButtonGCT>
            </Row>
          </Col>
          <Col className="relative" style={{ width: "500px" }}>
            <div
              className=" absolute flex justify-center items-center"
              style={{ width: "500px" }}
            >
              <img
                className="mt-6"
                src={G_FG}
                alt="img"
                style={{ width: "360px" }}
              />
            </div>
            <div
              className=" absolute flex justify-center items-center"
              style={{ width: "500px", top: "-40px" }}
            >
              <img src={G_BG} alt="img" style={{ width: "600px" }} />
            </div>
          </Col>
        </Row>
        
        <Row className="flex justify-start mt-64 mb-32">
          <Col
            className="simple-steps-wrap-bottom mr-16"
            style={{ width: "640px" }}
          >
            <img src={WindowsImage} alt="img" className="object-cover w-full" />
          </Col>
          <Col className="pr-12" style={{ width: "680px"}}>
            <Row>
              <Col>
                <Row>
                  <h2>How it</h2>
                  <h2 className="t-primary ml-4">works.</h2>
                </Row>
              </Col>
            </Row>
            <Row className="mt-4">
              <Col className="mr-6 btn-check-wrap">
                <img src={CheckIcon} alt="i" />
              </Col>
              <Col style={{ width: "560px" }}>
                <h5>Create an account and make a deposit</h5>
                <p className="t-subtitle">
                  Sign up in one easy step and make a deposit to your Wallet via
                  any payment method you like. If you already have funds in your
                  trading account at Gold Copytrade, you can add money to your
                  Wallet from it using Internal Transfer. Your Wallet balance
                  shows your uninvested funds.
                </p>
              </Col>
            </Row>
            <Row className="mt-4">
              <Col className="mr-6 btn-check-wrap">
                <img src={CheckIcon} alt="i" />
              </Col>
              <Col style={{ width: "560px" }}>
                <h5>Create Allocation</h5>
                <p className="t-subtitle">
                  Find Masters you want to follow and click ‘Copy’. Their
                  positions will be copied automatically. The deposit percentage
                  setting will help you manage your portfolio. Try copying
                  different strategies to decide which of them work best for
                  you!
                </p>
              </Col>
            </Row>
            <Row className="mt-4">
              <Col className="mr-6 btn-check-wrap">
                <img src={CheckIcon} alt="i" />
              </Col>
              <Col style={{ width: "560px" }}>
                <h5>Investors select the Traders to copy</h5>
                <p className="t-subtitle">
                  With an unlimited number of Provider and/or Provider+ to copy
                  you can create a balanced and diversified trading portfolio
                  and receive a stable income. You also have full control over
                  the process and can modify/stop copying trades at any given
                  time. You can view detailed trading statistics for copied
                  Provider and/or Provider+ in your Copier Area.
                </p>
              </Col>
            </Row>
          </Col>
        </Row>
      </Row>
      <JoinOur />
      <Footer />
    </PageWrapper>
  );
};
