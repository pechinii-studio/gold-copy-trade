import { Col, Row } from "antd";
import React from "react";
import { ButtonGCT } from "../../components/ButtonGCT";
import { Footer } from "../../components/Footer";
import { JoinOur } from "../../components/JoinOur";
import { NavBar } from "../../components/NavBar";
import { PageWrapper } from "../../components/PageWrappaer";
import play_icon from "../../Assets/icons/play-icon.svg";
import WindowsImage from "../../Assets/images/master-page/windows-img.png";
import CheckIcon from "../../Assets/icons/icon-check.svg";
import { Provider } from "../../components/Provider";
import { ProviderPlus } from "../../components/ProviderPlus";
import { VectorBg } from "../../components/VectorBG";
import Follower_BG from "../../Assets/images/follower-account-type/follower-bg.png";
import CurveBG from "../../Assets/vectors/bg-curve-master.svg";

export const FollowerPage = () => {
  return (
    <PageWrapper>
      <NavBar />
      <Row className="flex justify-center w-full">
        <VectorBg
          src={CurveBG}
          alt="img"
          style={{ top: "940px" }}
        />
      </Row>
      <Row>
        <VectorBg
          className="w-full absolute"
          src={Follower_BG}
          alt="img"
          style={{ top: "0px", right: "0px", zIndex: "0" }}
        />
      </Row>
      <Row className="mt-20 relative z-10" style={{ width: "1400px" }}>
        <Row className="w-full">
          <Col style={{ width: "540px" }}>
            <h2>Become Follower</h2>
            <p className="mt-6 t-subtitle">
              Profit by copying expert traders Gold Copytrade. This social
              trading platform allows you to follow the strategies of the top
              market performers in Gold Trading and copy them to earn money
              effortlessly. When professionals profit, you profit too!
            </p>
            <p className="mt-6 t-subtitle">
              Confusion and insecurity while trading are no more – now a huge
              professional community is by your side 24/7. Start today and let
              the selected experts work for you! We introduce you to the
              financial market and help to manage your investments wisely.
            </p>
          </Col>
        </Row>
        <Row className="flex mt-16 mb-52">
          <ButtonGCT className="mr-6 h-12">Invest Now</ButtonGCT>
          <ButtonGCT secondary className="flex items-center h-12">
            <img src={play_icon} alt="i" className="mr-2" />
            Learn more
          </ButtonGCT>
        </Row>

        <Row className="flex justify-start mt-32 mb-32">
          <Col
            className="simple-steps-wrap-bottom mr-16"
            style={{ width: "640px" }}
          >
            <img src={WindowsImage} alt="img" className="object-cover w-full" />
          </Col>
          <Col className="pr-12" style={{ width: "680px"}}>
            <Row>
              <Col>
                <Row>
                  <h2>How it</h2>
                  <h2 className="t-primary ml-4">works.</h2>
                </Row>
              </Col>
            </Row>
            <Row className="mt-4">
              <Col className="mr-6 btn-check-wrap">
                <img src={CheckIcon} alt="i" />
              </Col>
              <Col style={{ width: "560px" }}>
                <h5>Create an account and make a deposit</h5>
                <p className="t-subtitle">
                  Sign up in one easy step and make a deposit to your Wallet via
                  any payment method you like. If you already have funds in your
                  trading account at Gold Copytrade, you can add money to your
                  Wallet from it using Internal Transfer. Your Wallet balance
                  shows your uninvested funds.
                </p>
              </Col>
            </Row>
            <Row className="mt-4">
              <Col className="mr-6 btn-check-wrap">
                <img src={CheckIcon} alt="i" />
              </Col>
              <Col style={{ width: "560px" }}>
                <h5>Create Allocation</h5>
                <p className="t-subtitle">
                  Find Masters you want to follow and click ‘Copy’. Their
                  positions will be copied automatically. The deposit percentage
                  setting will help you manage your portfolio. Try copying
                  different strategies to decide which of them work best for
                  you!
                </p>
              </Col>
            </Row>
            <Row className="mt-4">
              <Col className="mr-6 btn-check-wrap">
                <img src={CheckIcon} alt="i" />
              </Col>
              <Col style={{ width: "560px" }}>
                <h5>Investors select the Traders to copy</h5>
                <p className="t-subtitle">
                  With an unlimited number of Provider and/or Provider+ to copy
                  you can create a balanced and diversified trading portfolio
                  and receive a stable income. You also have full control over
                  the process and can modify/stop copying trades at any given
                  time. You can view detailed trading statistics for copied
                  Provider and/or Provider+ in your Copier Area.
                </p>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="flex justify-center mb-24 relative z-10" style={{ width: "1400px" }}>
        <Provider />
        <ProviderPlus className="mt-10" />
      </Row>
      </Row>
      <JoinOur />
      <Footer />
    </PageWrapper>
  );
};
