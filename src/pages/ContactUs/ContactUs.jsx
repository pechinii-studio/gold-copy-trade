import { Col, Row } from "antd";
import React from "react";
import CurveBG from "../../Assets/vectors/bg-curve-master.svg";
import { Footer } from "../../components/Footer";
import { JoinOur } from "../../components/JoinOur";
import { NavBar } from "../../components/NavBar";
import { InputGCT } from "../../components/InputGCT";
import { TextAreaGCT } from "../../components/TextAreGCT";
import { ButtonGCT } from "../../components/ButtonGCT";
import { VectorBg } from "../../components/VectorBG";
import { PageWrapper } from "../../components/PageWrappaer";

const ContactUs = () => {
  return (
    <PageWrapper>
      <NavBar />
      <Row className="flex justify-center">
        <VectorBg
          src={CurveBG}
          alt="img"
          className="w-11/12"
          style={{ top: "260px" }}
        />
      </Row>

      <Row className="mt-20 relative z-10" style={{ height: "700px" }}>
        <Row className="w-full">
          <Col style={{ width: "720px" }}>
            <h2>Get in touch</h2>
            <div style={{ width: "300px" }}>
              <p className="t-subtitle">
                Let's connect to discuss how Gold copytrade can help you.{" "}
              </p>
            </div>
          </Col>
          <Col style={{ width: "540px" }}>
            <InputGCT
              className="mb-4"
              type="text"
              placeholder="Enter name"
              style={{ width: "500px" }}
            />
            <InputGCT
              className="mb-4"
              type="email"
              placeholder="Enter email"
              style={{ width: "500px" }}
            />
            <InputGCT
              className="mb-4"
              type="number"
              placeholder="Enter Phone"
              style={{ width: "500px" }}
            />
            <Col className="mb-6">
              <TextAreaGCT
                placeholder="Enter your message"
                style={{ width: "500px" }}
              />
            </Col>
            <ButtonGCT style={{ width: "160px" }}>Submit</ButtonGCT>
          </Col>
        </Row>
      </Row>
      <JoinOur />
      <Footer />
    </PageWrapper>
  );
};

export default ContactUs;
