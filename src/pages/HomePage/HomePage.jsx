import React from "react";
import "./HomePageStyles.css";
import { NavBar } from "../../components/NavBar";
import { Footer } from "../../components/Footer";
import styled from "styled-components";
import { OfferCard } from "../../components/OfferCard";
import { ButtonGCT } from "../../components/ButtonGCT";
import play_icon from "../../Assets/icons/play-icon.svg";
import bg_home_top from "../../Assets/vectors/bg-home-top.svg";
import bg_curve from "../../Assets/vectors/bg-curve.svg";
import bg_global from "../../Assets/vectors/bg-global.svg";

import { Row, Col } from "antd";
import { IoIosArrowForward, IoIosArrowBack } from "react-icons/io";
import { TraderCard } from "../../components/TraderCard";
import CheckIcon from "../../Assets/icons/icon-check.svg";
import Step01 from "../../Assets/images/homepage/homepage-simple-step-top.png";
import Step02 from "../../Assets/images/homepage/homepage-simple-step-bottom.png";
import Payment01 from "../../Assets/images/payment-01.png";
import Payment02 from "../../Assets/images/payment-02.png";
import Payment03 from "../../Assets/images/payment-03.png";
import Payment04 from "../../Assets/images/payment-04.png";
import Payment05 from "../../Assets/images/payment-05.png";
import Payment06 from "../../Assets/images/payment-06.png";
import WhyChooseIcon01 from "../../Assets/icons/keywords-icon.svg";
import WhyChooseIcon02 from "../../Assets/icons/regulated-icon.svg";
import WhyChooseIcon03 from "../../Assets/icons/segregated-icon.svg";
import WhyChooseIcon04 from "../../Assets/icons/support-icon.svg";
import Person01 from "../../Assets/images/person01.png";
import Person02 from "../../Assets/images/person02.png";
import Person03 from "../../Assets/images/person03.png";
import Person04 from "../../Assets/images/person04.png";
import ChartTrader01 from "../../Assets/vectors/chart-trader-01.svg";
import ChartTrader02 from "../../Assets/vectors/chart-trader-02.svg";
import ChartTrader03 from "../../Assets/vectors/chart-trader-03.svg";
import ChartTrader04 from "../../Assets/vectors/chart-trader-04.svg";
import { WhyChooseCard } from "../../components/WhyChooseCard";
import { OfferList } from "../../components/OfferList";
import { JoinOur } from "../../components/JoinOur";
import { LightFareBG } from "../../components/LightFareBG";
import { InnerContent } from "../../components/InnerContent";
import { VectorBg } from "../../components/VectorBG";

const HomePageContent = styled.div`
  height: 800px;
  position: relative;
`;

export const HomePage = () => {
  return (
    <div className="home-page-wrapper">
      <NavBar />
      <HomePageContent>
        <VectorBg src={bg_home_top} alt="img" className="w-5/6 right-0" />
        <VectorBg src={bg_curve} alt="img" style={{ top: "1400px" }} />
        <Row className="w-full flex justify-center">
          <VectorBg src={bg_global} alt="img" style={{ top: "2900px" }} />
        </Row>
        <LightFareBG style={{ top: "4000px", right: "2px" }} />

        <InnerContent>
          <Row className="mt-20" style={{ width: "680px" }}>
            <h1>The Fastest And Simplest Way To Trade Like A Pro</h1>
            <h5 className="t-subtitle" style={{ width: "340px" }}>
              Trade with confidence, Trade with ease. Let The best take the
              lead.
            </h5>
          </Row>
          <Row className="flex mt-10">
            <ButtonGCT className="mr-6">Invest Now</ButtonGCT>
            <ButtonGCT secondary className="flex items-center">
              <img src={play_icon} alt="i" className="mr-2" />
              Learn more
            </ButtonGCT>
          </Row>
        </InnerContent>
      </HomePageContent>
      <Row className="relative flex justify-center" style={{ height: "440px" }}>
        <InnerContent style={{ width: "1300px" }}>
          <Row>
            <Row className="w-full flex justify-between">
              <Col>
                <button className="featured-btn mb-5">Featured</button>
                <h2>Top rated Traders</h2>
              </Col>
              <Col className="flex">
                <div className="mr-4 arrow-btn">
                  <IoIosArrowBack size={24} />
                </div>
                <div className="arrow-btn">
                  <IoIosArrowForward size={24} />
                </div>
              </Col>
            </Row>
            <Row className="w-full flex justify-between mt-4">
              <TraderCard
                image={Person01}
                name="Emy Walter"
                username="@ FHubCopy"
                gain="88.17%"
                follower="10.5K"
                amount="$100K"
                chart={ChartTrader01}
              />
              <TraderCard
                image={Person02}
                name="Haworld Sam"
                username="@ HamerMan77"
                gain="78.58%"
                follower="10.25K"
                amount="$98.5K"
                chart={ChartTrader02}
              />
              <TraderCard
                image={Person03}
                name="Kara Danvers"
                username="@ GoldenLand11"
                gain="88.17%"
                follower="10.5K"
                amount="$100K"
                chart={ChartTrader03}
              />
              <TraderCard
                image={Person04}
                name="Sara Jackson"
                username="@ GoldenBear"
                gain="97.54%"
                follower="25.5K"
                amount="$300K"
                chart={ChartTrader04}
              />
            </Row>
          </Row>
        </InnerContent>
      </Row>
      <Row
        className="mt-20 relative flex justify-center"
        style={{ height: "560px" }}
      >
        <InnerContent style={{ width: "1300px" }}>
          <Row className="flex justify-between">
            <Col className="mt-10">
              <h2>Why Choose</h2>
              <h2 className="t-primary leading-4">Gold Copytrade</h2>
              <div className="mt-16" style={{ width: "580px" }}>
                <p className="t-subtitle">
                  Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                  accusantium doloremque laudantium, totam rem aperiam, eaque
                  ipsa quae ab illo inventore veritatis et quasi architecto
                  beatae vitae dicta sunt explicabo.
                </p>
                <p className="t-subtitle">
                  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
                  odit aut fugit, sed quia consequuntur magni dolores eos qui
                  ratione voluptatem sequi nesciunt.
                </p>
                <p className="t-subtitle">
                  Neque porro quisquam est, qui dolorem ipsum quia dolor sit
                  amet, consectetur, adipisci velit, sed quia non numquam eius
                  modi tempora incidunt ut labore et dolore magnam aliquam
                  quaerat voluptatem.
                </p>
              </div>
            </Col>
            <Col className="flex">
              <Col>
                <Row>
                  <WhyChooseCard
                    name="Keywords in domain name"
                    image={WhyChooseIcon01}
                  />
                </Row>
                <Row className="mt-8">
                  <WhyChooseCard
                    name="Regulated Broker"
                    image={WhyChooseIcon02}
                  />
                </Row>
              </Col>
              <Col>
                <Row className="mt-8 ml-8">
                  <WhyChooseCard
                    name="Segregated client funds"
                    image={WhyChooseIcon03}
                  />
                </Row>
                <Row className="mt-8 ml-8">
                  <WhyChooseCard
                    name="24/5 Client Support"
                    image={WhyChooseIcon04}
                  />
                </Row>
              </Col>
            </Col>
          </Row>
        </InnerContent>
      </Row>
      <Row
        className="mt-24 relative flex justify-center"
        style={{ height: "800px" }}
      >
        <InnerContent
          className="flex flex-col items-center"
          style={{ width: "1300px" }}
        >
          <Row className="flex w-full">
            <Col className="simple-steps-wrap-top mr-24">
              <img src={Step01} alt="img" className="object-cover w-full" />
            </Col>
            <Col className="mt-10">
              <h2>Simple steps to start</h2>
              <h2 className="t-primary leading-4">trade.</h2>
              <div className="mt-10" style={{ width: "450px" }}>
                <p className="t-subtitle">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam.
                </p>
              </div>
            </Col>
          </Row>
          <Row
            className="w-full flex justify-end absolute"
            style={{ bottom: "-260px" }}
          >
            <Col className="simple-steps-wrap-bottom mr-16">
              <img src={Step02} alt="img" className="object-cover w-full" />
            </Col>
            <Col className="mt-4 pr-12">
              <Row className="mt-4">
                <Col className="mr-6 btn-check-wrap">
                  <img src={CheckIcon} alt="i" />
                </Col>
                <Col>
                  <h5>Traders join Gold Copytrade</h5>
                  <p className="t-subtitle">
                    Connect their account and share their trading strategy
                  </p>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col className="mr-6 btn-check-wrap">
                  <img src={CheckIcon} alt="i" />
                </Col>
                <Col>
                  <h5>Gold Copytrade ranks Traders</h5>
                  <p className="t-subtitle">
                    Based on performance, stability, behavior and outlook
                  </p>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col className="mr-6 btn-check-wrap">
                  <img src={CheckIcon} alt="i" />
                </Col>
                <Col>
                  <h5>Investors select the Traders to copy</h5>
                  <p className="t-subtitle">
                    According to their investment capital and risk appetite
                  </p>
                </Col>
              </Row>
            </Col>
          </Row>
        </InnerContent>
      </Row>

      <Row
        className="mt-20 relative flex justify-center"
        style={{ height: "940px" }}
      >
        <InnerContent
          className="flex flex-col items-center"
          style={{ width: "1300px" }}
        >
          <h2>Account Types</h2>
          <h4 className="t-primary leading-3">Avail the best opportunity</h4>
          <div className="w-full mt-16 flex justify-between">
            <OfferCard className="mt-10">
              <Col className="w-full h-full relative">
                <div>
                  <h2 className="mt-4 leading-4">Founders</h2>
                  <h4 className="t-primary">Alpha Pass</h4>
                  <p className="t-subtitle mt-4 mb-8">
                    Unlock the collective power of market experts with exclusive
                    discord access, partnership perks, and calls.{" "}
                  </p>
                  <Col>
                    <OfferList name="Unrestricted Access to platform" />
                    <OfferList name="Community Discord Access" />
                    <OfferList name="Holders Chat Access" />
                    <OfferList name="Ecosystem Perks" />
                    <OfferList name="Partnership Perks" />
                    <OfferList name="Founders Club Discord Access" />
                    <OfferList name="Alpha Calls" />
                  </Col>
                </div>
                <div className="w-full flex flex-col items-center absolute bottom-4">
                  <h3 className="leading-4">0.03 ETH</h3>
                  <h4 className="t-primary">Lifetime</h4>
                  <ButtonGCT className="w-60 mt-6">Apply</ButtonGCT>
                </div>
              </Col>
            </OfferCard>
            <OfferCard primary>
              <Col className="w-full h-full relative">
                <div>
                  <h2 className="mt-4 leading-4">Lifetime</h2>
                  <p className="t-subtitle mt-6 mb-8">
                    Gain unrestricted access to our analytics platform and enjoy
                    the perks offered through Gold Coptrade.
                  </p>
                  <Col>
                    <OfferList name="Unrestricted Access to platform" />
                    <OfferList name="Community Discord Access" />
                    <OfferList name="Holders Chat Access" />
                    <OfferList name="Ecosystem Perks" />
                    <OfferList name="Partnership Perks" />
                    <OfferList
                      status={false}
                      name="Founders Club Discord Access"
                    />
                    <OfferList status={false} name="Alpha Calls" />
                  </Col>
                </div>
                <div className="w-full flex flex-col items-center absolute bottom-4">
                  <h3 className="leading-4">0.06 ETH</h3>
                  <ButtonGCT className="w-60 mt-6">Apply</ButtonGCT>
                </div>
              </Col>
            </OfferCard>
            <OfferCard className="mt-10">
              <Col className="w-full h-full relative">
                <div>
                  <h2 className="mt-4 leading-4">Monthly</h2>
                  <p className="t-subtitle mt-4 mb-8">
                    Try our platform and see for yourself what the hype is all
                    about.
                  </p>
                  <Col>
                    <OfferList name="Unrestricted Access to platform" />
                    <OfferList name="Community Discord Access" />
                    <OfferList status={false} name="Holders Chat Access" />
                    <OfferList status={false} name="Ecosystem Perks" />
                    <OfferList status={false} name="Partnership Perks" />
                    <OfferList
                      status={false}
                      name="Founders Club Discord Access"
                    />
                    <OfferList status={false} name="Alpha Calls" />
                  </Col>
                </div>
                <div className="w-full flex flex-col items-center absolute bottom-4">
                  <h3 className="leading-4">0.15 ETH</h3>
                  <h4 className="t-primary">0.05 ETH per month</h4>
                  <ButtonGCT className="w-60 mt-6">Apply</ButtonGCT>
                </div>
              </Col>
            </OfferCard>
          </div>
        </InnerContent>
      </Row>
      <Row
        className="mt-20 relative flex justify-center"
        style={{ height: "390px" }}
      >
        <InnerContent
          className="flex flex-col items-center"
          style={{ width: "1300px" }}
        >
          <h2>Payment Methods</h2>
          <Row className="mt-4">
            <Col className="m-4">
              <img src={Payment01} alt="img" className="object-cover" />
            </Col>
            <Col className="m-4">
              <img src={Payment02} alt="img" className="object-cover" />
            </Col>
            <Col className="m-4">
              <img src={Payment03} alt="img" className="object-cover" />
            </Col>
            <Col className="m-4">
              <img src={Payment04} alt="img" className="object-cover" />
            </Col>
            <Col className="m-4">
              <img src={Payment05} alt="img" className="object-cover" />
            </Col>
            <Col className="m-4">
              <img src={Payment06} alt="img" className="object-cover" />
            </Col>
          </Row>
        </InnerContent>
      </Row>
      <JoinOur />
      <Footer />
    </div>
  );
};
