const CracoLessPlugin = require("craco-less");

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              "@primary-color": "#9E7D26", // primary color for all components
              "@link-color": "#9E7D26", // link color
              "@success-color": "#52c41a", // success state color
              "@warning-color": "#faad14", // warning state color
              "@error-color": "#f5222d", // error state color
              "@font-size-base": "14px", // major text font size
              "@heading-color": "#FFFFFF", // heading text color
              "@text-color": "#FFFFFF", // major text color
              "@text-color-secondary": "#AEAEAE", // secondary text color
              "@disabled-color": "rgba(0, 0, 0, 0.25)", // disable state color
              "@border-radius-base": "2px", // major border radius
              "@border-color-base": "#d9d9d9", // major border color

              "@btn-danger-color": "#fff",
              "@btn-danger-bg": "rgba(206, 183, 125, 0.15)",
              "@btn-danger-border": "rgba(206, 183, 125, 0.15)",



              "@menu-bg": "#191505",
              "@menu-dark-color": "#F0F0F0",
              "@menu-dark-danger-color": "@error-color",
              "@menu-dark-bg": "#312911",
              "@menu-dark-arrow-color":" #fff",
              "@menu-dark-inline-submenu-bg": "#000c17",
              "@menu-dark-highlight-color": "#fff",
              "@menu-dark-item-active-bg":"transparent",
              "@menu-dark-item-active-danger-bg": "@error-color",
              "@menu-dark-selected-item-icon-color": "@white",
              "@menu-dark-selected-item-text-color": "@white",
              "@menu-dark-item-hover-bg": "transparent",
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
